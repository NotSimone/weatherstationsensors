// HTS221 Temperature / Humidity Sensor
using Unosquare.RaspberryIO;
using Unosquare.RaspberryIO.Abstractions;
using Unosquare.WiringPi;

namespace WeatherStationSensors {
    public class HTS221 : Sensor {
        public int addr {get;}
        public string name {get; set;} = "HTS221";
        private Measurement[] avail_measurements =
            new Measurement[] {
                new Measurement(MeasurementType.Temperature, "C"),
                new Measurement(MeasurementType.Humidity, "%")
            };
        // I2C library device reference
        private II2CDevice dev;

        // Calibration values
        private float H0_rH, H1_rH, T0_degC, T1_degC;
        private int H0_OUT, H1_OUT, T0_OUT, T1_OUT;

        // Constructor linking to i2c address
        public HTS221(int address) {
            addr = address;
            // Initialise RaspberryIO
            Pi.Init<BootstrapWiringPi>();
            // Initialise the temp sensor
            dev = Pi.I2C.AddDevice(addr);
            // Check the device is valid
            try {
                if (dev.ReadAddressByte(0x0F) != 0xBC)
                    throw new System.ArgumentException("Device is not a HTS221", "address");
            } catch (Unosquare.RaspberryIO.Abstractions.Native.HardwareException e) {
                throw new System.ArgumentException("Hardware error - is this a valid I2C address?", e);
            }
            // Average configuration register
            dev.WriteAddressByte(0x10, 0x1B);
            // Control register 1
            dev.WriteAddressByte(0x20, 0x85);
            // Setup calibration - ref. datasheet
            H0_rH = dev.ReadAddressByte(0x30) / 2;
            H1_rH = dev.ReadAddressByte(0x31) / 2;
            H0_OUT = (dev.ReadAddressByte(0x37) << 8) | dev.ReadAddressByte(0x36);
            H1_OUT = (dev.ReadAddressByte(0x3B) << 8) | dev.ReadAddressByte(0x3A);
            int msb = dev.ReadAddressByte(0x35) & 0xF;
            T0_degC = (((msb & 0x3) << 8) | dev.ReadAddressByte(0x32)) / 8;
            T1_degC = (((msb & 0xC) << 6) | dev.ReadAddressByte(0x33)) / 8;
            T0_OUT = (dev.ReadAddressByte(0x3D) << 8) | dev.ReadAddressByte(0x3C);
            T1_OUT = (dev.ReadAddressByte(0x3F) << 8) | dev.ReadAddressByte(0x3E);
        }

        // Return an array of available measurement types
        public Measurement[] AvailMeasurements() {
            return avail_measurements;
        }

        // Sense for the measurement type
        public float SenseData(MeasurementType type) {
            switch(type) {
                // y = m(x-x_0)+y_0
                case MeasurementType.Temperature:
                    int T_OUT = (dev.ReadAddressByte(0x2B) << 8) | dev.ReadAddressByte(0x2A);
                    return (T1_degC - T0_degC) / (T1_OUT - T0_OUT) * (T_OUT - T0_OUT) + T0_degC;
                case MeasurementType.Humidity:
                    int H_OUT = (dev.ReadAddressByte(0x29) << 8) | dev.ReadAddressByte(0x28);
                    return (H1_rH - H0_rH) / (H1_OUT - H0_OUT) * (H_OUT - H0_OUT) + H0_rH;
                default:
                    return -1;
            }
        }
    }
}