// Interface and Type Definitions
namespace WeatherStationSensors {
    public interface Sensor {
        Measurement[] AvailMeasurements();
        float SenseData(MeasurementType type);
        int addr {get;}
        string name {get; set;}
    }

    public class Measurement {
        public MeasurementType type {get;}
        public string unit {get;}
        
        public Measurement(MeasurementType new_type, string new_unit) {
            type = new_type;
            unit = new_unit;
        }
    }

    public enum MeasurementType {
        Temperature,
        Humidity,
        Pressure
    }

}