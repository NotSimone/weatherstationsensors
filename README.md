# WeatherStationSensors

## About

Simple sensor interfaces implemented with the Raspberry Pi Sense Hat sensors using the [RaspberryIO](https://unosquare.github.io/raspberryio/) library. Intended for use with my WeatherStation GUI.

Currently implementing the HTS221 temperature/humidity sensor and the LPS25H temperature/pressure sensor.

Written in C# .Net Core.

## Requirements

Raspberry Pi running some flavour of Linux. The default sensor config is based on the Sense Hat but you can change this easily yourself.

## Compilation

Requires .Net Core SDK 3.1.  

Run `dotnet publish` in the directory to automatically build with the csproj.
