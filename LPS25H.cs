// LPS25H Temperature / Pressure Sensor
using Unosquare.RaspberryIO;
using Unosquare.RaspberryIO.Abstractions;
using Unosquare.WiringPi;

namespace WeatherStationSensors {
    public class LPS25H : Sensor {
        public int addr {get;}
        public string name {get; set;} = "LPS25H";
        private Measurement[] avail_measurements =
            new Measurement[] {
                new Measurement(MeasurementType.Temperature, "C"),
                new Measurement(MeasurementType.Pressure, "hPa")
            };
        // I2C library device reference
        private II2CDevice dev;

        // Constructor linking to i2c address
        public LPS25H(int address) {
            addr = address;
            // Initialise RaspberryIO
            Pi.Init<BootstrapWiringPi>();
            // Initialise the temp sensor
            dev = Pi.I2C.AddDevice(addr);
            // Check the device is valid
            try {
                if (dev.ReadAddressByte(0x0F) != 0xBD)
                    throw new System.ArgumentException("Device is not a LPS25H", "address");
            } catch (Unosquare.RaspberryIO.Abstractions.Native.HardwareException e) {
                throw new System.ArgumentException("Hardware error - is this a valid I2C address?", e);
            }
            // Average configuration register
            dev.WriteAddressByte(0x10, 0x1B);
            // Control register 1
            dev.WriteAddressByte(0x20, 0xA0);
        }

        // Return an array of available measurement types
        public Measurement[] AvailMeasurements() {
            return avail_measurements;
        }

        // Sense for the measurement type
        public float SenseData(MeasurementType type) {
            switch(type) {
                // Values are received as 2's complement and may need fixing
                case MeasurementType.Temperature:
                    int temp_raw = (dev.ReadAddressByte(0x2C) << 8) | dev.ReadAddressByte(0x2B);
                    if ((temp_raw & (0b1 << 15)) != 0)
                        temp_raw -= 0b1 << 16;
                    return temp_raw / 480 + 42.5f;
                case MeasurementType.Pressure:
                    int press_raw = (dev.ReadAddressByte(0x2A) << 16) | (dev.ReadAddressByte(0x29) << 8) | dev.ReadAddressByte(0x28);
                    if ((press_raw & (0b1 << 23)) != 0)
                        press_raw -= 0b1 << 24;
                    return press_raw / 4096;
                default:
                    return -1;
            }
        }
    }
}